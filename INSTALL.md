Building Bitnetcoin
================

See doc/build-*.md for instructions on building the various
elements of the Bitnetcoin Core reference implementation of Bitnetcoin.
